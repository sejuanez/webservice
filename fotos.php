<?php
// Web Service
error_reporting(0);
include("gestion.php");

$array = file_get_contents('php://input');
$array = (json_decode($array, true));

$return_arr = array();
$sql = "";

$primerCampo = "";
$segundoCampo = "";
$flang = true;

foreach ($array as $json) {


    $id = $json['ID'];
    $acta = $json['CUENTA'];
    $nFoto = $json['NUMERO'];
    $anio = $json['ANIO'];
    $mes = $json['MES'];
    $base = $json['FOTO64'];


    $nombre = $id . "-" . $acta . "-" . $nFoto . ".png";
    $ruta = "../evidencias/" . $anio . "-" . $mes . "/";

    $resultImg = base64_to_jpeg($base, $ruta, $nombre);


    $stmt = "EXECUTE PROCEDURE FOTO_VISITAS_CREA ( 
                                                    '" . $id . "',
                                                    '" . $acta . "',  
                                                    '" . $nFoto . "',  
                                                    '" . $anio . "',  
                                                    '" . $mes . "')";

    $query = ibase_prepare($stmt);
    $resultQuery = ibase_execute($query);

    if (!$resultQuery) {
        $resultQuery = "" . str_replace('"', '', ibase_errmsg()) . "";
    }

    $row_array_err['img'] = $resultImg;
    $row_array_err['query'] = $resultQuery;
    array_push($return_arr, $row_array_err);

    echo json_encode($return_arr);

}

function base64_to_jpeg($base64, $ruta, $nombreArchivo)
{

    //create dir
    mkdir($ruta, 0700, true);

    // open the output file for writing
    if (file_exists($ruta . $nombreArchivo)) {
        unlink($ruta . $nombreArchivo);
        $ifp = fopen($ruta . $nombreArchivo, 'x');
    } else {
        $ifp = fopen($ruta . $nombreArchivo, 'x');

    }

    //echo $base64;
    //echo $ruta . $nombreArchivo;

    // split the string on commas
    // $data[ 0 ] == "data:image / png;base64"
    // $data[ 1 ] == <actual base64 string>


    // we could add validation here with ensuring count( $data ) > 1
    //fwrite($ifp, $base64);

    $result = file_put_contents($ruta . $nombreArchivo, base64_decode($base64));
    fclose($ifp);

    if ($result) {
        return 'Imagen guardada';
    } else {
        return 'No se pudo guardar la imagen';
    }

}


