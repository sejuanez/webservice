<?php
// Web Service
error_reporting(0);
include("gestion.php");

$array = file_get_contents('php://input');
$array = (json_decode($array, true));

//var_dump($array);


$return_arr = array();
$sql = "";

$primerCampo = "";
$segundoCampo = "";
$campos = 0;

foreach ($array as $json) {

    $sql = "SELECT * FROM ";

    $sql .= $json['procedimiento'];

    $sqlCampos = 'SELECT
                    RDB$PROCEDURE_OUTPUTS
                  FROM
                    RDB$PROCEDURES
                  WHERE
                    RDB$PROCEDURE_NAME =' . "'" . $json['procedimiento'] . "'";

    $result = ibase_query($sqlCampos);

    while ($fila = ibase_fetch_row($result)) {
        $campos = $fila[0];
    }


    $sql .= " (";

    for ($i = 1; $i <= count($json['valores']); $i++) {

        if ($i == 1) {
            $primerCampo = utf8_decode($json['valores'][$i]);
        }

        if ($i == count($json['valores'])) {

            if ($json['valores'][$i] == 'null') {
                $sql .= "" . utf8_decode($json['valores'][$i]) . "";
                $sql .= ");";
            } else {
                $sql .= "'" . utf8_decode($json['valores'][$i]) . "'";
                $sql .= ");";
            }

        } else {

            if ($json['valores'][$i] == 'null') {
                $sql .= "" . utf8_decode($json['valores'][$i]) . "";
                $sql .= ",";
            } else {
                $sql .= "'" . utf8_decode($json['valores'][$i]) . "'";
                $sql .= ",";
            }

        }
    }

    $query = ibase_prepare($sql);
    $result = ibase_execute($query);
    $return_arr = array();


    while ($fila = ibase_fetch_row($result)) {
        $row_array = array();
        for ($i = 0; $i < $campos; $i++) {
            $row_array[$i] = utf8_encode($fila[$i]);
        }
        array_push($return_arr, $row_array);
    }

    $response = array(
        "resultado" => $return_arr
    );

    $response2 = array(
        "menu" => $response
    );

    echo json_encode($response2);

}


